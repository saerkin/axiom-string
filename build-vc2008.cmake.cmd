@mkdir target\build-vc2008
@pushd target\build-vc2008
cmake -G "Visual Studio 9 2008" ..\..\
cmake --build . --config Release --target ALL_BUILD
ctest -C Release
@popd