# noinspection PyUnresolvedReferences
from conans import ConanFile, CMake

class AxiomStringConan(ConanFile):
  name = "axiom-string"
  version = "0.1.0"
  license = "MIT"
  author = "Sergey A. Erkin (sergey.a.erkin@gmail.com)"
  url = "https://saerkin@bitbucket.org/saerkin/axiom-string.git"
  settings = "os", "compiler", "build_type", "arch"
  exports_sources = "include/*"
  no_copy_source = True
  generators = "cmake"
  requires = \
    "axiom-ptr/0.1.0@sergey-erkin/experimental"
  build_requires = \
    "Catch/1.12.2@bincrafters/stable"

  def package(self):
    self.copy("*.h")

  def package_id(self):
    self.info.header_only()
