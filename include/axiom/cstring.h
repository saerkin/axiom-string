/******************************************************************************/
/* MIT License                                                                */
/*                                                                            */
/* Copyright (c) 2022 Sergey A Erkin                                          */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom          */
/* the Software is furnished to do so, subject to the following conditions:   */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included    */
/* in all copies or substantial portions of the Software.                     */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#pragma once

#include <axiom/shared_ptr.h>
#include <cstdlib>
#include <cstdarg>
#include <cstring>
#include <cassert>
#include <cstdio>

#ifndef AXIOM_CSTRING_FORMAT_BUFF_SIZE
#define AXIOM_CSTRING_FORMAT_BUFF_SIZE 256
#endif

namespace axiom {

/**
 * Immutable (except operators =, +=) string consists of ANSI-only chars
 */
class cstring {
// Nested classes
private:
  struct Str {
    char* chars; // non null
    int length;

    inline Str() : chars(_strdup("")), length(0) {}

    inline Str(char* chars, int length) : chars(chars), length(length) {
      assert(length >= 0);
      if (this->chars == NULL)
        this->chars = _strdup("");
    }

    inline ~Str() {
      free(chars);
    }
  };
  shared_ptr<Str> p;

  // Constructors
public:
  inline cstring() : p(new Str()) {}

  inline cstring(const char chars[]) : p(new Str()) { // NOLINT(google-explicit-constructor)
    if (chars != NULL) {
      p = shared_ptr<Str>(
        new Str(
          _strdup(chars),
          strlen(chars) // NOLINT(bugprone-narrowing-conversions,cppcoreguidelines-narrowing-conversions)
        )
      );
    }
  }

  inline cstring(const cstring& other) : p(other.p) {}

private:
  inline cstring(char* chars, int length) : p(new Str(chars, length)) {}


// Operators
public:
  inline cstring& operator=(const cstring& other) {
    if (this != &other) {
      p = other.p;
    }
    return *this;
  }

  inline bool operator==(const cstring& other) const {
    return equals(other);
  }

  inline bool operator==(const char* other) const {
    return equals(other);
  }

  inline bool operator!=(const cstring& other) const {
    return !equals(other);
  }

  inline bool operator!=(const char* other) const {
    return !equals(other);
  }

  inline bool operator>=(const cstring& other) const {
    return compare(other) >= 0;
  }

  inline bool operator<=(const cstring& other) const {
    return compare(other) <= 0;
  }

  inline bool operator>(const cstring& other) const {
    return compare(other) > 0;
  }

  inline bool operator<(const cstring& other) const {
    return compare(other) < 0;
  }

  inline cstring operator+(const cstring& other) const {
    return concat(other);
  }

  inline cstring& operator+=(const cstring& other) {
    return *this = concat(other);
  }

  inline operator const char*() const { // NOLINT(google-explicit-constructor)
    return chars();
  }

// Methods
public:
  inline int length() const {
    return p->length;
  }

  inline bool isEmpty() const {
    return 0 == p->length;
  }

  inline const char* chars() const {
    return p->chars;
  }

  inline int compare(const cstring& other) const {
    return strcmp(p->chars, other.p->chars);
  }

  inline int compareIgnoreCase(const cstring& other) const {
    return _stricmp(p->chars, other.p->chars);
  }

  inline bool equals(const cstring& other) const {
    return 0 == compare(other);
  }

  inline bool equalsIgnoreCase(const cstring& other) const {
    return 0 == compareIgnoreCase(other);
  }

  inline cstring concat(const cstring& other) const {
    if (isEmpty())
      return other;
    if (other.isEmpty())
      return *this;

    int length = this->length() + other.length();
    char* chars = (char*) malloc(length + 1);
    memcpy(chars, this->chars(), this->length());
    memcpy(chars + this->length(), other.chars(), other.length());
    chars[length] = 0;

    return cstring(chars, length);
  }
};

inline cstring coalesce(const cstring& s1, const cstring& s2) {
  if (!s1.isEmpty())
    return s1;
  return s2;
}

inline cstring coalesce(const cstring& s1, const cstring& s2, const cstring& s3) {
  if (!s1.isEmpty())
    return s1;
  if (!s2.isEmpty())
    return s2;
  return s3;
}

inline cstring format(const char* format, ...) {
  va_list args;
    va_start(args, format);
  char buff[AXIOM_CSTRING_FORMAT_BUFF_SIZE] = { 0 };
  vsnprintf(buff, AXIOM_CSTRING_FORMAT_BUFF_SIZE - 1, format, args); // NOLINT(cert-err33-c)
    va_end (args);
  return cstring(buff);
}

inline cstring operator+(const cstring& str, int number) {
  char buff[100] = { 0 }; // NOLINT(readability-magic-numbers)
  _itoa(number, buff, 10); // NOLINT(readability-magic-numbers)
  return str + buff;
}

} // end of axiom