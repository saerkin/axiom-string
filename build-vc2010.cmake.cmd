@mkdir target\build-vc2010
@pushd target\build-vc2010
cmake -G "Visual Studio 10 2010" ..\..\
cmake --build . --config Release --target ALL_BUILD
ctest -C Release
@popd