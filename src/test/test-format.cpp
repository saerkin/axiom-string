#include <catch.hpp>
#include <axiom/cstring.h>

using namespace axiom;

TEST_CASE("format() composes string and number", "[axiom::format()]") {
  REQUIRE(format("String %s, number %d", "str", 125) == "String str, number 125");
}

TEST_CASE("format() adds leading zeros to int", "[axiom::format()]") {
  REQUIRE(format("%05d", 4) == "00004");
}