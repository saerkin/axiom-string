#include <catch.hpp>
#include <axiom/cstring.h>

using namespace axiom;

TEST_CASE("cstring can be constructed without params", "[axiom::cstring]") {
  cstring str;
  REQUIRE(str.isEmpty());
}

TEST_CASE("cstring can be constructed from const char*", "[axiom::cstring]") {
  cstring str = "test";
}

TEST_CASE("cstring can be copied", "[axiom::cstring]") {
  cstring str = "test";
  cstring copy1 = str; // NOLINT(performance-unnecessary-copy-initialization)

  cstring copy2;
  copy2 = str;

  REQUIRE(str == copy1);
  REQUIRE(str == copy2);
}

TEST_CASE("cstring can be casted to const char*", "[axiom::cstring]") {
  cstring str = "test";
  REQUIRE(0 == strcmp(str.chars(), "test"));
}

TEST_CASE("cstring has length", "[axiom::cstring]") {
  cstring str = "test";
  REQUIRE(str.length() == 4);

  cstring empty;
  REQUIRE(empty.isEmpty());

  cstring nonempty = "nonempty";
  REQUIRE(!nonempty.isEmpty());
}

TEST_CASE("cstring can be compared", "[axiom::cstring]") {
  cstring string1 = "string1";
  cstring string2 = "string2";

  REQUIRE(!string1.equals(string2));
  REQUIRE(string1.compare(string2) != 0);
  REQUIRE(string1.compareIgnoreCase(string2) != 0);
  REQUIRE(string1 != string2);

  cstring string1_copy = "string1";
  REQUIRE(string1.equals(string1_copy));
  REQUIRE(string1.compare(string1_copy) == 0);
  REQUIRE(string1.compareIgnoreCase(string1_copy) == 0);
  REQUIRE(string1 == string1_copy);

  cstring string2_case_insensitive_copy = "String2";
  REQUIRE(!string2.equals(string2_case_insensitive_copy));
  REQUIRE(string2.compare(string2_case_insensitive_copy) != 0);
  REQUIRE(string2.equalsIgnoreCase(string2_case_insensitive_copy));
  REQUIRE(string2.compareIgnoreCase(string2_case_insensitive_copy) == 0);

  cstring string3 = "string30";
  REQUIRE(string1 < string2);
  REQUIRE(string3 > string1);
  REQUIRE(string1 >= string1_copy);
  REQUIRE(string1 <= string1_copy);
}

TEST_CASE("cstring can be concatenated", "[axiom::cstring]") {
  cstring string1 = "one";
  cstring string2 = "two";

  REQUIRE((string1 + " " + string2) == "one two");
  REQUIRE((cstring("") += string1 + " " + string2) == "one two");
  REQUIRE(string1 == "one");
  REQUIRE(string2 == "two");
}

TEST_CASE("cstring can be concatenated with int", "[axiom::cstring]") {
  cstring string1 = "one ";
  cstring string2 = " two";

  REQUIRE((string1 + 255) == "one 255");
  REQUIRE((string1 + 255 + string2) == "one 255 two");
}
