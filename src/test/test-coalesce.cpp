#include <catch.hpp>
#include <axiom/cstring.h>

using namespace axiom;

TEST_CASE("coalesce() with two operands", "[axiom::coalesce()]") {
  REQUIRE(coalesce("", "") == "");
  REQUIRE(coalesce("first", "") == "first");
  REQUIRE(coalesce("", "second") == "second");
  REQUIRE(coalesce("first", "second") == "first");
}

TEST_CASE("coalesce() with three operands", "[axiom::coalesce()]") {
  REQUIRE(coalesce("", "", "") == "");
  REQUIRE(coalesce("first", "", "") == "first");
  REQUIRE(coalesce("", "second", "") == "second");
  REQUIRE(coalesce("", "", "third") == "third");
  REQUIRE(coalesce("first", "second", "") == "first");
  REQUIRE(coalesce("", "second", "third") == "second");
  REQUIRE(coalesce("first", "", "third") == "first");
  REQUIRE(coalesce("first", "second", "third") == "first");
}
